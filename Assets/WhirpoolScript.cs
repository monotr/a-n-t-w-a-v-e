﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhirpoolScript : MonoBehaviour {
	public float rotationSpeed;
	private float curRot;

	void Start () {
		curRot = transform.localRotation.eulerAngles.y;
	}

	void Update () {
		curRot += rotationSpeed * Time.deltaTime;
		transform.rotation = Quaternion.Euler (0, curRot, 0);
	}
}
