﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEmmiterScript : MonoBehaviour {
	public AudioSource myAudioSource;
	public AudioClip[] myClips;

	public void playSound (int soundID) {
		myAudioSource.clip = myClips [soundID];

		if (soundID == 2) {
			myAudioSource.time = 9;
			Destroy (gameObject, myClips [soundID].length - 9);
		} else {
			Destroy (gameObject, myClips [soundID].length);
		}
		myAudioSource.Play ();
	}
}
