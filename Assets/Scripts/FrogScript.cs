﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogScript : MonoBehaviour {
	private float minLenguaSize;
	public float maxLenguaSize;
	public float timeBetweenLenguetazo;
	public float lenguetazoSpeed;
	public GameObject toungueObj;
	private bool isSacandoToungue = true;
	private bool isAttacking = true;
	public bool isRotating = false;
	public float rotationSpeed;
	private float curRot;

	public Animator frogAnimator;

	//movement
	public float frequency = 1.0f; // in Hz
	public Vector3 positionA;
	public Vector3 positionB;
	private float elapsedTime = 0.0f;
	public bool isMoving = false;

	public GameObject soundEmmiterObj;

	void Start () {
		curRot = transform.localRotation.eulerAngles.y;
		minLenguaSize = toungueObj.transform.localScale.z;
	}

	void FixedUpdate(){
		if (isSacandoToungue && isAttacking) {
			toungueObj.transform.localScale += Vector3.forward * lenguetazoSpeed;
			frogAnimator.SetBool ("isLick", true);
			if (toungueObj.transform.localScale.z >= maxLenguaSize) {
				isSacandoToungue = false;
			}

			GameObject newEmmiter = Instantiate (soundEmmiterObj, transform.position, Quaternion.identity) as GameObject;
			newEmmiter.GetComponent<SoundEmmiterScript> ().playSound (2);
		} else if(!isSacandoToungue && isAttacking) {
			toungueObj.transform.localScale -= Vector3.forward * lenguetazoSpeed;

			if (toungueObj.transform.localScale.z <= minLenguaSize) {
				frogAnimator.SetBool ("isLick", false);
				isAttacking = false;
				Invoke ("changeDirection", timeBetweenLenguetazo);
			}
		}
	}

	void Update(){
		if (isRotating) {
			curRot += rotationSpeed * Time.deltaTime;
			transform.rotation = Quaternion.Euler (0, curRot, 0);
		}

		if (isMoving) {
			elapsedTime += Time.deltaTime;
			float cosineValue = Mathf.Cos(2.0f * Mathf.PI * frequency * elapsedTime);
			transform.position = positionA + (positionB - positionA) * 0.5f * (1 - cosineValue);
		}
	}

	void changeDirection(){
		isAttacking = true;
		isSacandoToungue = true;
	}

}
