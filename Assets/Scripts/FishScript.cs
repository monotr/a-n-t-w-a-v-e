﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishScript : MonoBehaviour {
	private Vector3 startPosition;
	private Vector3 direction;
	public float speed;

	public float directionTimer;

	private Quaternion newRot;

	public bool isFollowingPlayer = false;
	public Vector3 playerToFollowPos;

	private float time; // and/or direction
	private Vector3 crossDirection = Vector3.one; // More important for a 3D approach, but still a good point of reference

	public Renderer fishRenderer;
	public Material[] fishMats;

	void Start() {
		InvokeRepeating ("changeDir", 0, directionTimer);

		fishRenderer = GetComponent<Renderer> ();
	}

	void Update() {
		if (fishRenderer.isVisible) {
			transform.rotation = Quaternion.Lerp (transform.rotation, newRot, Time.deltaTime);

			transform.position = startPosition + (direction * speed * time) + (crossDirection * Mathf.Sin (time));
			time += Time.deltaTime;

			if (isFollowingPlayer && speed < 6)
				speed += Time.deltaTime * 3;
			else if (!isFollowingPlayer && speed > 3)
				speed -= Time.deltaTime * 5;

			if (isFollowingPlayer) {
				fishRenderer.material = fishMats [1];
			} else {
				fishRenderer.material = fishMats [0];
			}
		}
	}
	void FixedUpdate(){
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, 8.5f)) {
			//print (hit.distance + " / " + hit.collider.tag);
			if (hit.collider.tag == "Rock" || hit.collider.tag == "Whirpool") {
				CancelInvoke ();
				InvokeRepeating ("changeDir", 0, directionTimer);
			}
		}
	}

	void changeDir(){
		Transform auxTrans = transform;

		if (!isFollowingPlayer) {
			float x = Random.Range (-50, 50);
			float z = Random.Range (-50, 50);

			auxTrans.LookAt (new Vector3 (x, -4, z));
			newRot = auxTrans.rotation;

			direction = auxTrans.forward;
			direction = direction.normalized;

			time = 0.0f;
			crossDirection = new Vector3 (direction.z, 0, -direction.x); // A quick right angle for 2D
			startPosition = transform.position;
		} else {
			auxTrans.LookAt (new Vector3 (playerToFollowPos.x, 0, playerToFollowPos.z));
			newRot = auxTrans.rotation;

			direction = auxTrans.forward;
			direction = direction.normalized;

			time = 0.0f;
			crossDirection = new Vector3 (direction.z, 0, -direction.x); // A quick right angle for 2D
			startPosition = transform.position;
		}
	}

	public void callInvoke(){
		transform.position -= transform.forward;
		direction = -transform.forward;
		direction = direction.normalized;
		//CancelInvoke ();
		//InvokeRepeating ("changeDir", 0, directionTimer);
	}

	/*void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Whirpool") {
			callInvoke ();
		}
	}

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.tag == "Rock") {
			callInvoke ();
		}
	}*/
}
