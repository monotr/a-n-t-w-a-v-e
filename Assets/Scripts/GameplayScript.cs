﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameplayScript : MonoBehaviour {
	public GameObject boatObj;
	//private Rigidbody boatRb;
	public GameObject waterDropObj;
	public float power, radius;
	private List<GameObject> fishList;
	[HideInInspector] public float fishDanger = 0;
	public Slider dangerSlider;
	private float maxDanger = 6;
	public float minDistanceToFollow;
	[HideInInspector] public Vector3 startPos;

	public Slider hpSlider;
	public float playerHP;
	public float maxHP;

	public Image[] coinsImages;
	public int totalCoins;

	public bool passedLevel = false;
	public GameObject gameOverObj;
	public Text[] gameOverTxts;
	private float roundTime;

	private int curScene;

	void Start(){
		fishList = new List<GameObject> ();
		fishList.AddRange(GameObject.FindGameObjectsWithTag ("Fish"));
		print (fishList.Count);
		playerHP = maxHP;
		hpSlider.maxValue = maxHP;
		hpSlider.value = maxHP;
		//boatRb = boatObj.GetComponent<Rigidbody> ();

		dangerSlider.maxValue = maxDanger;
		dangerSlider.value = 0;

		curScene = SceneManager.GetActiveScene().buildIndex;
	}

	void Update(){
		if (!passedLevel && playerHP > 0) {
			roundTime += Time.deltaTime;

			if (Input.GetMouseButtonDown (0)) {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;

				if (Physics.Raycast (ray, out hit, 100)) {
					if (hit.collider.tag == "Water") {
						fishDanger += 2;
						Instantiate (waterDropObj, hit.point, Quaternion.identity);
					}
				}
			}

			if (fishDanger > 0) {
				fishDanger -= Time.deltaTime * 2;
			}

			/*if (fishDanger > 3) {
				foreach (GameObject f in fishList) {
					if (Vector3.Distance (boatObj.transform.position, f.transform.position) < minDistanceToFollow) {
						FishScript fishScript = f.GetComponent<FishScript> ();
						fishScript.fishRenderer.material = fishScript.fishMats [2];
					}
				}
			}*/
			if (fishDanger > maxDanger) {
				foreach (GameObject f in fishList) {
					//print (Vector3.Distance (boatObj.transform.position, f.transform.position));
					if (Vector3.Distance (boatObj.transform.position, f.transform.position) < minDistanceToFollow) {
						FishScript fishScript = f.GetComponent<FishScript> ();
						fishScript.isFollowingPlayer = true;
						fishScript.playerToFollowPos = boatObj.transform.position;
						//fishScript.speed = 10;
					}
				}
			} else {
				foreach (GameObject f in fishList) {
					FishScript fishScript = f.GetComponent<FishScript> ();
					fishScript.isFollowingPlayer = false;
					//fishScript.speed = 3;
				}
			}

			dangerSlider.value = fishDanger;
		}
	}

	public void alterHP(float hpDif){
		playerHP += hpDif;
		if (playerHP > maxHP)
			playerHP = maxHP;
		hpSlider.value = playerHP;

		if (playerHP <= 0)
			youDied ();
	}

	public void youDied(){
		boatObj.GetComponent<Rigidbody> ().useGravity = false;
		fishDanger = 0;
		//playerHP = maxHP;
		//boatObj.transform.position = startPos;
		showGameOverPanel ();
		//print ("you are so death bitch");
	}

	public void showGameOverPanel(){
		playerHP = 0;
		gameOverObj.SetActive (true);
		if (passedLevel) {
			if (curScene != 2) {
				gameOverTxts [0].text = "Hurray!!!\n" + "Total time: " + roundTime + "\nCoins: " + totalCoins;
				gameOverTxts [1].text = "Next";
			} else {
				gameOverTxts [0].text = "Thank you for playing! :D More levels coming soon";
				Button[] allButs = GameObject.FindObjectsOfType<Button> ();
				foreach (Button b in allButs) {
					b.gameObject.SetActive(false);
				}
			}
		} else {
			gameOverTxts [0].text = "Failed! :(";
			gameOverTxts [1].text = "Restart";
		}
	}

	public void gameOverBut(bool isMenu){
		if (isMenu)
			SceneManager.LoadScene (0, LoadSceneMode.Single);
		else {
			if (passedLevel)
				SceneManager.LoadScene (curScene + 1, LoadSceneMode.Single);
			else
				SceneManager.LoadScene (curScene, LoadSceneMode.Single);
		}
	}
}
