﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDropScript : MonoBehaviour {
	public float maxScale;
	public float increaseSpeed;
	public bool isDamaged = false;

	public GameObject soundEmmiterObj;

	void FixedUpdate () {
		if (transform.position.y <= 0.6f) {
			GameObject newEmmiter = Instantiate (soundEmmiterObj, transform.position, Quaternion.identity) as GameObject;
			newEmmiter.GetComponent<SoundEmmiterScript> ().playSound (0);

			if (GetComponent<Rigidbody> ())
				Destroy (GetComponent<Rigidbody> ());

			GetComponent<SphereCollider> ().enabled = true;

			Vector3 newScale = transform.localScale + Vector3.one * increaseSpeed;
			transform.localScale = new Vector3 (newScale.x, 0.3f, newScale.z);
			increaseSpeed -= 0.05f;
		}

		if (transform.localScale.x >= maxScale)
			Destroy (gameObject);
	}
}
