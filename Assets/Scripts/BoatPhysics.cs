﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BoatTutorial
{
	public class BoatPhysics : MonoBehaviour
	{
		//Drags
		public GameObject underWaterObj;

		//Script that's doing everything needed with the boat mesh, such as finding out which part is above the water
		private ModifyBoatMesh modifyBoatMesh;

		//Mesh for debugging
		private Mesh underWaterMesh;

		//The boats rigidbody
		private Rigidbody boatRB;

		//The density of the water the boat is traveling in
		private float rhoWater = 1027f;

		public float whirpoolIntensity;
		public float whirpoolDragForce;
		private Camera mainCam;
		private GameplayScript gameScript;
		public GameObject antObj;
		private bool gotAnt = false;

		private AudioSource mySound;
		public AudioClip[] myClips;

		public GameObject soundEmmiterObj;

		void Start() {
			//Get the boat's rigidbody
			boatRB = gameObject.GetComponent<Rigidbody>();

			//Init the script that will modify the boat mesh
			modifyBoatMesh = new ModifyBoatMesh(gameObject);

			//Meshes that are below and above the water
			underWaterMesh = underWaterObj.GetComponent<MeshFilter>().mesh;

			mainCam = Camera.main;

			gameScript = mainCam.GetComponent<GameplayScript> ();
			gameScript.startPos = transform.position;

			mySound = GetComponent<AudioSource> ();
		}

		void Update() {
			//Generate the under water mesh
			modifyBoatMesh.GenerateUnderwaterMesh();

			//Display the under water mesh
			modifyBoatMesh.DisplayMesh(underWaterMesh, "UnderWater Mesh", modifyBoatMesh.underWaterTriangleData);

			mainCam.transform.position = new Vector3 (transform.position.x, mainCam.transform.position.y, transform.position.z -15);

			gameScript.alterHP (-Time.deltaTime);
		}

		void FixedUpdate() {
			//Add forces to the part of the boat that's below the water
			if (modifyBoatMesh.underWaterTriangleData.Count > 0)
			{
				AddUnderWaterForces();
			}

			if (gameScript.playerHP > 0) {
				RaycastHit hit;
				if (Physics.Raycast (transform.position, transform.up, out hit)) {
					//print ("arriba está " + hit.collider.name);
					if (hit.point.y < -2) {
						print ("died by volteado");
						gameScript.showGameOverPanel ();

						//playSound
						mySound.clip = myClips[1];
						mySound.time = 10.0f;
						mySound.Play ();
					}
				}
			}
		}

		//Add all forces that act on the squares below the water
		void AddUnderWaterForces() {
			//Get all triangles
			List<TriangleData> underWaterTriangleData = modifyBoatMesh.underWaterTriangleData;

			for (int i = 0; i < underWaterTriangleData.Count; i++)
			{
				//This triangle
				TriangleData triangleData = underWaterTriangleData[i];

				//Calculate the buoyancy force
				Vector3 buoyancyForce = BuoyancyForce(rhoWater, triangleData);

				//Add the force to the boat
				boatRB.AddForceAtPosition(buoyancyForce, triangleData.center);


				//Debug

				//Normal
				Debug.DrawRay(triangleData.center, triangleData.normal * 3f, Color.white);

				//Buoyancy
				Debug.DrawRay(triangleData.center, buoyancyForce.normalized * -3f, Color.blue);
			}
		}

		//The buoyancy force so the boat can float
		private Vector3 BuoyancyForce(float rho, TriangleData triangleData) {
			//Buoyancy is a hydrostatic force - it's there even if the water isn't flowing or if the boat stays still

			// F_buoyancy = rho * g * V
			// rho - density of the mediaum you are in
			// g - gravity
			// V - volume of fluid directly above the curved surface 

			// V = z * S * n 
			// z - distance to surface
			// S - surface area
			// n - normal to the surface
			Vector3 buoyancyForce = rho * Physics.gravity.y * triangleData.distanceToSurface * triangleData.area * triangleData.normal;

			//The vertical component of the hydrostatic forces don't cancel out but the horizontal do
			buoyancyForce.x = 0f;
			buoyancyForce.z = 0f;

			return buoyancyForce;
		}

		void OnCollisionEnter(Collision other) {
			if (other.gameObject.tag == "Rock") {
				GameObject newEmmiter = Instantiate (soundEmmiterObj, Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity) as GameObject;
				newEmmiter.GetComponent<SoundEmmiterScript> ().playSound (1);

				gameScript.alterHP (-20);
			} else if (other.gameObject.tag == "WaterDrop" && other.gameObject.GetComponent<WaterDropScript> ().isDamaged) {
				gameScript.alterHP (-10);
			} else if (other.gameObject.tag == "Toungue" && !gotAnt) {
				gotAnt = true;
				antObj.transform.SetParent (other.gameObject.transform);
				antObj.transform.position = other.contacts [0].point;
				Invoke ("willDie", 3.0f);
				print ("dead by frog");
			} else if (other.gameObject.tag == "Fish") {
				Invoke ("willDie", 3.0f);
				print ("dead by fish");
				FishScript fishScript = other.gameObject.GetComponent<FishScript> ();
				fishScript.isFollowingPlayer = false;
				fishScript.callInvoke ();
			}
		}

		void OnTriggerEnter(Collider other) {
			if (other.gameObject.tag == "Item") {
				ItemsScript itemScript = other.gameObject.GetComponent<ItemsScript> ();

				//playSound
				itemScript.mySound.clip = itemScript.myClips[itemScript.itemType];
				itemScript.mySound.Play();

				switch (itemScript.itemType) {
				case 0:  // --- COIN
					gameScript.coinsImages [itemScript.coinNum].enabled = true;
					gameScript.totalCoins++;
					break;
				case 1: // --- HP
					gameScript.alterHP (60);
					break;
				}
				Destroy (other.gameObject);
			} else if (other.gameObject.tag == "FinishLine") {
				

				gameScript.passedLevel = true;
				print ("finish!!!");
				gameScript.showGameOverPanel ();
			}
		}

		void willDie(){
			//playSound
			mySound.clip = myClips[0];
			mySound.time = 7.0f;
			mySound.Play ();

			gameScript.youDied ();

			gotAnt = false;

			antObj.transform.SetParent (transform);
			antObj.transform.localPosition = new Vector3(0, 6, 0);
		}

		void OnTriggerStay(Collider other) {
			if (other.tag == "Whirpool") {
				//print ("OMFG A FKNG WHIRPOOL!!!!!! D: // Distance from center: " + Vector3.Distance (transform.position, other.gameObject.transform.position));
				transform.RotateAround(other.gameObject.transform.position, Vector3.up, Time.deltaTime * whirpoolIntensity);
				transform.position = Vector3.MoveTowards (transform.position, other.gameObject.transform.position, Time.deltaTime * whirpoolDragForce);

				gameScript.alterHP (-Time.deltaTime * 3);

				if (Vector3.Distance (transform.position, other.gameObject.transform.position) < 1) {
					gameScript.youDied ();
				}
			}
		}
	}
}