﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropperScript : MonoBehaviour {
	public GameObject dropObj;
	public float dropTimer;
	public Transform spawnTrans;

	void Start () {
		InvokeRepeating ("spawnDrop", dropTimer, dropTimer);
	}

	void spawnDrop(){
		GameObject newDrop = Instantiate (dropObj, spawnTrans.position, Quaternion.identity) as GameObject;
		newDrop.AddComponent<Rigidbody>();
		newDrop.GetComponent<WaterDropScript> ().isDamaged = true;
	}

}
