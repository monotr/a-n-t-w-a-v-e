﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsScript : MonoBehaviour {
	public float rotationSpeed;
	private float curRot;
	public int itemType;
	public GameObject[] itemsObj;
	public int coinNum;

	[HideInInspector] public AudioSource mySound;
	public AudioClip[] myClips;

	void Start () {
		for (int i = 0; i < itemsObj.Length; i++) {
			if (i == itemType)
				itemsObj [i].SetActive (true);
		}

		curRot = transform.localRotation.eulerAngles.y;

		mySound = GetComponent<AudioSource> ();
		mySound.clip = myClips [itemType];
	}

	void Update(){
		curRot += rotationSpeed * Time.deltaTime;
		transform.rotation = Quaternion.Euler (0, curRot, 0);
	}
}
